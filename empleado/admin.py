from django.contrib import admin
from datetime import date
from .models import *
# Register your 

@admin.register(Empleado)
class EmpleadoAdmin(admin.ModelAdmin):
	search_fields = ('nombre','apellido','identificacion','celular')
	list_display = ('identificacion','nombre','apellido','celular')

@admin.register(Familiar)
class FamiliarAdmin(admin.ModelAdmin):
    search_fields = ('nombre','apellido','parentesco')
    list_display = ('nombre','apellido','parentesco')