from django.db import models


TIPO_DOCUMENTO = (
    ('LC', 'Libreta Civica'),
    ('LE', 'Libreta de Enrolamiento'),
    ('DNI', 'Documento Nacional de Identidad'),
)

SEXO_CHOICE = (
	('F', 'Femenino'),
	('M', 'Masculino'),
)

ESTADO_CIVIL = (
	('SOLTERO', 'Soltera/o'),
	('CASADO', 'Casada/o'),
	('DIVORCIADO', 'Divorciada/o'),
	('VIUDO', 'Viuda/o'),
)
BOOL=(
	('SI', 'SI'),
	('NO', 'NO'),
)
PARENTESCO=(
	('MADRE','Madre'),
	('PADRE','Padre'),
	('HIJO','Hija/o'),
	('CONYUGE','Conyuge'),
	('ESPOSO','Esposa/o'),
)
# Create your models here.
class Persona(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    tipoDocumento = models.CharField( max_length=5, choices=TIPO_DOCUMENTO, null=True)
    nroDocumento = models.PositiveIntegerField(unique= True)
    fechaNacimiento = models.DateField(verbose_name="Fecha de Nacimiento",null=True)
    calleDireccion = models.CharField(max_length=50, blank=True, null=True, verbose_name="Calle")
    nroDireccion = models.CharField(max_length=10, blank=True, null=True, verbose_name="Nº")
    class Meta:
        abstract = True
    def __str__(self):
    	return self.apellido+", "+ self.nombre

class Familiar(Persona):
	parentesco = models.CharField(max_length=10, choices=PARENTESCO, null=True)
	vive = models.CharField(max_length=3, choices=BOOL, null=True)
	class Meta:
		verbose_name = "Familiar"
		verbose_name_plural = "Familiares"

class Empleado(Persona):
	identificacion = models.CharField(max_length=15, null=True)
	fechaIngreso = models.DateField(verbose_name="Fecha de Ingreso")
	CUIL = models.CharField(max_length=15, null=True)
	sexo = models.CharField( max_length=10, choices=SEXO_CHOICE, null=True)
	lugarNacimiento = models.CharField(max_length=50, blank=True, null=True, verbose_name="Lugar de Nacimiento")
	estadoCivil = models.CharField( max_length=10, choices=ESTADO_CIVIL, null=True)
	telefono = models.CharField(max_length=20, blank=True, null=True, verbose_name="Teléfono Fijo")
	celular = models.CharField(max_length=20, blank=True, null=True, verbose_name="Celular")
	email = models.EmailField(max_length=20, blank=True, null=True, verbose_name="Correo electrónico")
	familiares = models.ManyToManyField(Familiar, verbose_name="Lista de Familiares")
	class Meta:
		verbose_name = "Empleado"
		verbose_name_plural = "Empleados"
