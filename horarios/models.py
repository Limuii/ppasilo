from django.db import models
import datetime
from django.core.validators import MinValueValidator
from .models import *
from empleado.models import *

tipoES_CHOICE = (
    ('C/In', 'Entró'),
    ('C/Out', 'Salió'),
)
VERIFICACION_CHOICE = (
    ('FINGERPRINT', 'Huella digital'),
)
turnoSemana_CHOICE = (
    ('0', 'Mañana'),
    ('1', 'Tarde'),
    ('2', 'Noche'),
    ('3', 'Franco'),
    ('4', 'Limpieza'),
    ('5', 'Cocina'),
    ('6', 'Baño'),
)

#Aca definimos las horas que trabajara por dia en una semana para cada turno
class Semana(models.Model):     
    id = models.SmallIntegerField(primary_key=True,validators=[MinValueValidator(0)])
    descripcion = models.CharField(max_length=2, choices=turnoSemana_CHOICE)
    lunesEntrada = models.TimeField(null=False)
    lunesHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    martesEntrada = models.TimeField(null=False)
    martesHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    miercolesEntrada = models.TimeField(null=False)
    miercolesHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    juevesEntrada = models.TimeField(null=False)
    juevesHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    viernesEntrada = models.TimeField(null=False)
    viernesHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    sabadoEntrada = models.TimeField(null=False)
    sabadoHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])
    domingoEntrada = models.TimeField(null=False)
    domingoHoras = models.SmallIntegerField(null=False,validators=[MinValueValidator(0)])

    class Meta:
        verbose_name = "Horas por turno"
        verbose_name_plural = "Horas por turno"

    def __str__(self):
        return self.id.__str__()

class Feriado(models.Model):
    id = models.SmallIntegerField(primary_key=True,validators=[MinValueValidator(0)])
    descripcion = models.CharField(max_length=30,null=False)
    fecha = models.DateField(null=False)
    
    class Meta:
        verbose_name = "Feriado"
        verbose_name_plural = "Feriados"

    def __str__(self):
        return self.id.__str__()

class Certificado(models.Model):
	empleado= models.ForeignKey(Empleado, on_delete=models.CASCADE, verbose_name="Empleado/a")
	descripcion= models.CharField(max_length=30, null=True)
	fechaInicio = models.DateField(null=False)
	fechaFin = models.DateField(null=False)

	def __str__(self):
		return self.empleado.__str__()

class Turno(models.Model):
    id = models.SmallIntegerField(primary_key=True,validators=[MinValueValidator(0)])
    descripcion = models.CharField(max_length=30,null=False)
    semana = models.OneToOneField(Semana,on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "Turno"
        verbose_name_plural = "Turnos"

    def __str__(self):
        return self.id.__str__()

class Registro(models.Model):
    empleado = models.PositiveIntegerField(null=True)
    departamento = models.CharField(max_length=30)
    nombre = models.CharField(max_length=30, null=True)
    id = models.DateTimeField(primary_key=True)
    tipoVerificacion = models.CharField(max_length=20, choices=VERIFICACION_CHOICE)#FINGERPRINT
    tipoES = models.CharField(max_length=6, choices=tipoES_CHOICE) #C/IN -- C/OUT
    
    class Meta:
        verbose_name = "Registro"
        verbose_name_plural = "Registros"

    def __str__(self):
        return self.id.__str__()

class CalendarioPersona(models.Model):
	id = models.CharField(primary_key=True, max_length=1, default='1', unique=True, editable=False)
	fecha = models.DateField(editable=True,verbose_name="Período")
	empleado= models.ForeignKey(Empleado, on_delete=models.CASCADE, verbose_name="Empleado/a")

	class Meta:
		verbose_name = "Calendario Personal"
		verbose_name_plural = "Calendario Personal"

	def __str__(self):
		return self.fecha.__str__()

class Grupo(models.Model):
	id = models.PositiveSmallIntegerField(primary_key=True)
	primerisimoTurno= models.CharField(max_length=2, choices=turnoSemana_CHOICE)
	nombre = models.CharField(max_length=15, null=True)
	turno = models.OneToOneField(Turno, on_delete=models.CASCADE, verbose_name="Turno del Grupo")
	array_empleado = models.ManyToManyField(Empleado, verbose_name="Empleados del Grupo") 

	class Meta:
		verbose_name = "Grupo"
		verbose_name_plural = "Grupos"
	
	def __str__(self):
		return self.nombre

class CalendarioGrupal(models.Model):
	id = models.PositiveSmallIntegerField(default=1, editable=False, primary_key=True)
	fecha = models.DateField(editable=True,verbose_name="Período")
	grupos = models.ManyToManyField(Grupo, verbose_name="Grupos del Mes")

	class Meta:
		verbose_name = "Calendario Grupal"
		verbose_name_plural = "Calendario de Grupos"

	def __str__(self):
		return self.fecha.__str__()
