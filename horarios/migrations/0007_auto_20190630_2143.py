# Generated by Django 2.0.13 on 2019-06-30 21:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horarios', '0006_auto_20190628_2242'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GraficarCalendarioGrupal',
        ),
        migrations.AddField(
            model_name='grupo',
            name='primerisimoTurno',
            field=models.CharField(default=0, max_length=2),
        ),
        migrations.AlterField(
            model_name='calendariogrupal',
            name='id',
            field=models.PositiveSmallIntegerField(default=1, editable=False, primary_key=True, serialize=False),
        ),
    ]
