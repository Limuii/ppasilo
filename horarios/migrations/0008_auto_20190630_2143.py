# Generated by Django 2.0.13 on 2019-06-30 21:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horarios', '0007_auto_20190630_2143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grupo',
            name='primerisimoTurno',
            field=models.CharField(choices=[('0', 'Mañana'), ('1', 'Tarde'), ('2', 'Noche'), ('3', 'Franco'), ('4', 'Limpieza'), ('5', 'Cocina'), ('6', 'Baño')], max_length=2),
        ),
    ]
