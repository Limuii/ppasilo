from django.contrib import admin
from datetime import *
from import_export.admin import ImportExportModelAdmin
from .models import *

# Register your models here.
@admin.register(Turno)
class TurnoAdmin(admin.ModelAdmin):
	search_fields = ('id','descripcion')
	list_display = ('id','descripcion')

@admin.register(Feriado)
class FeriadoAdmin(admin.ModelAdmin):
	search_fields = ('id','fecha')
	list_display = ('id','fecha','descripcion')

@admin.register(Semana)
class SemanaAdmin(admin.ModelAdmin):
	search_fields = ('id','descripcion')
	list_display = ('id','descripcion')

@admin.register(Grupo)
class GrupoAdmin(admin.ModelAdmin):
	search_fields = ('id','nombre','turno')
	list_display = ('id','nombre','turno')

@admin.register(Certificado)
class CertificadoAdmin(admin.ModelAdmin):
    search_fields = ('empleado__apellido', 'fechaInicio')
    list_display = ('empleado','fechaInicio','fechaFin')

@admin.register(Registro)
class ViewAdmin(ImportExportModelAdmin):
	earch_fields = ('id', 'nombre', 'empleado', 'tipoES')
	list_display = ('id', 'nombre', 'empleado', 'tipoES')
	"""change_list_template = 'admin/registroView.html'
	def changelist_view(self, request, extra_context=None):
		super()
		extra_context= extra_context or {}
		extra_context['registros']= Registro.objects.all()
		return super(ImportExportModelAdmin, self).changelist_view(request, extra_context=extra_context)"""
	
#Uso una clase calendario para hacer la vista y eso
@admin.register(CalendarioPersona)
class CalendarioPAdmin(admin.ModelAdmin):
	change_list_template = 'admin/calendarioPersona.html'
	def changelist_view(self, request, extra_context=None):
		extra_context= extra_context or {}
		extra_context['calendario']=CalendarioPersona.objects.all()
    ########################################### SECCION ESTRUCTURA DE CALENDARIO############################################
        #Define el último día de cada mes, para que la vista sepa hasta donde hacer el calendario ###
		for x in extra_context['calendario']:			#x será siempre una unica instancia
			meses=('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre')
			extra_context['mes']=meses[x.fecha.month - 1]
			mes=x.fecha.month
			anio=x.fecha.year
			if mes in [1,3,5,7,8,10,12]:
				ultimoDia=31
			if mes in [4,6,9,11]:
				ultimoDia=30
			if mes == 2:
				if anio % 4 == 0:
					if anio % 100 == 0:
						if anio % 400 == 0:
							ultimoDia=29
						else:
							ultimoDia=28
					else:
						ultimoDia=29
				else:
					ultimoDia=28
			extra_context['ultimoDia'] = ultimoDia
        #Defino para cada día, su correspondiente dia de la semana
			extra_context['registro']=Registro.objects.all().filter(empleado=x.empleado.identificacion).order_by('id')
			semana=0
			int_dia=0
			list_dia=[]
			####armo tupla de registro
			fechaAuxiliar=x.fecha.replace(day=1)
			auxES='C/Out'
			tupla=[]
			suppertupla=[]
			for r in extra_context['registro']:
				if fechaAuxiliar == r.id.date() and r.tipoES=='C/In': #SI ESTA ENTRANDO son iguales las fechas
					if auxES =='C/Out': #Si esta en el mismo dia y entra despues de salir, es recreo
						##agrego a una misma tupla
						tupla.append(r)
						auxES='C/In'

					elif auxES=='C/In':#Si esta en el mismo dia y entra despues entrar, pisa el registro 
						###No hacemos
						auxES='C/In'

				elif fechaAuxiliar == r.id.date() and r.tipoES=='C/Out':#SI ESTA SALIENDO son iguales las fechas
					if auxES=='C/In':#Si esta en el mismo dia y sale despues de entrar, es recreo o termina el turno
						##AGREGAS APPEND
						tupla.append(r)
						auxES='C/Out'

					elif auxES=='C/Out':#Si esta en el mismo dia y sale despues de salir, pisa la salida turno
						##AGREGAS APPEND
						tupla.append(r)
						auxES='C/Out'

				elif fechaAuxiliar != r.id.date() and r.tipoES=='C/Out':#SI ESTA SALIENDO y son distintas las fechas
					auxdate=r.id - timedelta(days=1)
					if fechaAuxiliar == auxdate.date() :#diferencia de 1 dia es el caso de la noche
						if auxES=='C/In':#Si esta en distinto dia y sale despues de entrar, es recreo o termina el turno
							##AGREGAS APPEND
							tupla.append(r)
							auxES='C/Out'

						elif auxES=='C/Out':#Si esta en distinto dia y sale despues de salir, pisa la salida turno
							##AGREGAS APPEND
							tupla.append(r)
							auxES='C/Out'

					elif auxdate.date() > fechaAuxiliar and auxdate.date().month == fechaAuxiliar.month :
						##guardo tupla q venia creando en la super tupla
						if not tupla == []:
							suppertupla.append(tupla)
						##vacio la tupla
						tupla=[]
						#append tupla
						tupla.append(r)
						fechaAuxiliar=r.id.date()
						auxES='C/Out'
					
				elif fechaAuxiliar != r.id.date() and r.tipoES=='C/In':#SI ESTA Entrando y son distintas las fechas
					auxdate=r.id - timedelta(days=1)

					if fechaAuxiliar == auxdate.date() :#diferencia de 1 dia
						if auxES =='C/Out' and r.id.time()>datetime.time(5,30): #Si cambia de dia despues de salir, comienza un nuevo turno
							##guardo tupla q venia creando en la super tupla
							if not tupla == []:
								suppertupla.append(tupla)
							##vacio la tupla
							tupla=[]
							#append tupla
							tupla.append(r)
							fechaAuxiliar=r.id.date()
							auxES='C/In'

						elif auxES =='C/Out' and datetime.time(5,0)>r.id.time(): #Si cambia de dia despues de salir, pero no 
							#esta cerca de terminar el turno seguimos cargando
							##AGREGAS APPEND
							tupla.append(r)
							auxES='C/In'

						elif auxES=='C/In' and datetime.time(5,0)>r.id.time():#Si estas en distinto dia y entra despues entrar, pero no paso la hora piso registro 
							###No hacemos nada repite entrada
						
							auxES='C/In'
					
					elif auxdate.date() > fechaAuxiliar and auxdate.date().month == fechaAuxiliar.month :
						##guardo tupla q venia creando en la super tupla
						if not tupla == []:
							suppertupla.append(tupla)
						##vacio la tupla
						
						tupla=[]
						#append tupla
						tupla.append(r)
						fechaAuxiliar=r.id.date()
						auxES='C/In'
			##########################PASO TUPLA FINAL BORRAR
			suppertupla.append(tupla)
			##########################ARMO TUPLA DE VALORES FINALES DEL REGISTRO
			auxtupla=suppertupla
			contRec=0
			contTar=0
			horasTot=0
			registrosTarde=[]
			infotupla=[]
			recreo=[]
			
			for t in auxtupla:
				horas=0
				auxhoras=0
				auxminutos=0
				bandera=0
				turno='hola'
				if len(t) > 0:				
					auxRegistro=t[0]
					dia=auxRegistro.id.date()
					if auxRegistro.id.time() > datetime.time(5,00) and auxRegistro.id.time() < datetime.time(13,00):
						turno='Mañana'
						if auxRegistro.id.time() > datetime.time(6,10):
							registrosTarde.append(t[0])
							contTar+=1
					elif auxRegistro.id.time() > datetime.time(13,00) and auxRegistro.id.time() < datetime.time(21,00):
						turno='Tarde'
						if auxRegistro.id.time() > datetime.time(14,10):
							registrosTarde.append(t[0])
							contTar+=1
					elif auxRegistro.id.time() > datetime.time(21,00):
						turno='Noche'
						if auxRegistro.id.time() > datetime.time(22,10):
							registrosTarde.append(t[0])
							contTar+=1
					t.remove(t[0])
					while bandera != 1:
						if not t:
							if horas==0:
								horas='No completo E/S'
								auxminutos=''
							bandera=1
						elif auxRegistro.tipoES=='C/In':
							if t[0].tipoES=='C/In':
								###no hago nada porque se pisa el registro
								t.remove(t[0])
							elif t[0].tipoES=='C/Out':
								###entra despues de recreo
								#calculo diferencia
								auxhoras=t[0].id - auxRegistro.id
								horas= horas + auxhoras.total_seconds() / 3600
								auxRegistro=t[0]
								t.remove(t[0])
						elif auxRegistro.tipoES=='C/Out':
							if t[0].tipoES=='C/In':
								###entra despues de recreo
								recreo.append(t[0])
								contRec+=1
								auxRegistro=t[0]
								t.remove(t[0])
							elif t[0].tipoES=='C/Out':
								#calculo diferencia
								auxhoras=t[0].id - auxRegistro.id
								if (auxhoras.total_seconds() / 3600) > 1:
									horas=horas+auxhoras.total_seconds() / 3600
								print(horas)
								auxRegistro=t[0]
								t.remove(t[0])
					if horas != 'No completo E/S': 
						horasTot=horasTot+horas
						auxminutos=horas-int(horas)
						horas=int(horas)
						auxminutos=int(auxminutos*60)
					info=dia,turno,horas,auxminutos
					infotupla.append(info)

			if horasTot != 0:
				auxminutos=horasTot-int(horasTot)
				horasTot=int(horasTot)
				auxminutos=int(auxminutos*60)
			extra_context['turnos']=infotupla			
			extra_context['diasTar']=registrosTarde
			extra_context['llegadasTar']=contTar
			extra_context['recreos']=contRec
			extra_context['horas_totales']=horasTot
			extra_context['min_totales']=auxminutos
			auxCont=0
			tope=len(infotupla)
			for it_dia in range(1, ultimoDia+1):
				fechaAuxiliar=x.fecha.replace(day=it_dia)
				a = it_dia
				b= fechaAuxiliar.isoweekday()
				#########Logica Registro###########
				if auxCont < tope:
					if infotupla[auxCont][0].day==it_dia:
						c=a,b,infotupla[auxCont][1], infotupla[auxCont][2], infotupla[auxCont][3]
						auxCont+=1
					else:
						c=a,b,0,0	
				else:
					c=a,b,0,0
				list_dia.append(c)
			extra_context['listDay']=list_dia
			extra_context['diasRecreo']=recreo

			certificados=Certificado.objects.all().filter(empleado=x.empleado)
			periodoCertificado=[]

			for cert in certificados:
				if (x.fecha.month == cert.fechaInicio.month and x.fecha.year == cert.fechaInicio.year) or (x.fecha.month == cert.fechaFin.month and x.fecha.year == cert.fechaFin.year):
					periodoCertificado.append(cert)
			if len(periodoCertificado)==0:
				periodoCertificado.append('No presento certificados este mes')
			extra_context['certificados']=periodoCertificado


			diasCertificados=[]
			#Permite agarrar cada uno de los dias y pasarlo en certificados, o al menos contar uno por uno
			for cert in certificados:
				inicio=cert.fechaInicio
				fin= cert.fechaFin + timedelta(days=1)
				print("Fin= "+ str(fin))
				while inicio < fin:
					print(inicio)
					if x.fecha.month == inicio.month and x.fecha.year == inicio.year:
						diasCertificados.append(inicio)
					inicio = inicio + timedelta(days=1)
			extra_context['cantidadCertificados']=len(diasCertificados)

	########################################################################################################################
		return super(CalendarioPAdmin, self).changelist_view(request, extra_context=extra_context)  


@admin.register(CalendarioGrupal)
class GrCalendarioGrupalAdmin(admin.ModelAdmin):
    change_list_template = 'admin/calendarioGrupo.html'
    def changelist_view(self, request, extra_context=None):
        extra_context= extra_context or {}
        extra_context['calendario']=CalendarioGrupal.objects.all()
    ########################################### SECCION ESTRUCTURA DE CALENDARIO############################################
        #Define el último día de cada mes, para que la vista sepa hasta donde hacer el calendario ###
        for x in extra_context['calendario']:			#x será siempre una unica instancia
        	meses=('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre')
        	extra_context['mes']=meses[x.fecha.month - 1]
        	mes=x.fecha.month
        	anio=x.fecha.year
        	if mes in [1,3,5,7,8,10,12]:
        		ultimoDia=31
        	if mes in [4,6,9,11]:
        		ultimoDia=30
        	if mes == 2:
        		if anio % 4 == 0:
        			if anio % 100 == 0:
        				if anio % 400 == 0:
        					ultimoDia=29
        				else:
        					ultimoDia=28
        			else:
        				ultimoDia=29
        		else:
        			ultimoDia=28
        	extra_context['ultimoDia'] = ultimoDia
        	fechaPrimerisima= datetime.date(day=31,month=12,year=2018)
        	indiceRotacion=int((x.fecha.replace(day=1)-fechaPrimerisima).days/7)
        	print("Diferencia en dias= "+ str((x.fecha.replace(day=1)-fechaPrimerisima).days))
        	print("Indice de rotacion=" + str(indiceRotacion))
        	while indiceRotacion > 3:
        		indiceRotacion=indiceRotacion-4
        	print("Indice de Rotaciòn: "+ str(indiceRotacion))
        	if indiceRotacion == 0:
        		g1= 'manana'
        		g2= 'tarde'
        		g3= 'noche'
        		g4= 'franco'
        	elif indiceRotacion == 1:
        		g1= 'tarde'
        		g2= 'noche'
        		g3= 'franco'
        		g4= 'manana'
        	elif indiceRotacion == 2:
        		g1= 'noche'
        		g2= 'franco'
        		g3= 'manana'
        		g4= 'tarde'
        	elif indiceRotacion == 3:
        		g1= 'franco'
        		g2= 'manana'
        		g3= 'tarde'
        		g4= 'noche'
        	#Defino para cada día, su correspondiente dia de la semana
        	list_dia=[]
        	semana=0
        	for it_dia in range(1, ultimoDia+1):
	        	fechaAuxiliar=x.fecha.replace(day=it_dia)
	        	a= it_dia
	        	b= fechaAuxiliar.isoweekday()
	        	if b == 7:
        			semana=semana+1
	        	c=a,b,semana
	        	list_dia.append(c)
        	extra_context['listDay']=list_dia
        	t=[]
        	if g1 == 'manana':
        		t.append('g1')
        	elif g2 == 'manana':
        		t.append('g2')
        	elif g3 == 'manana': 
        		t.append('g3')
        	elif g4 == 'manana': 
        		t.append('g4')

        	if g1 == 'tarde':
        		t.append('g1')
        	elif g2 == 'tarde':
        		t.append('g2')
        	elif g3 == 'tarde': 
        		t.append('g3')
        	elif g4 == 'tarde': 
        		t.append('g4')

        	if g1 == 'noche':
        		t.append('g1')
        	elif g2 == 'noche':
        		t.append('g2')
        	elif g3 == 'noche': 
        		t.append('g3')
        	elif g4 == 'noche': 
        		t.append('g4')

        	if g1 == 'franco':
        		t.append('g1')
        	elif g2 == 'franco':
        		t.append('g2')
        	elif g3 == 'franco': 
        		t.append('g3')
        	elif g4 == 'franco': 
        		t.append('g4')
       	
	       	
        	extra_context['semana0']=t
        	p=[]
        	p=(t[3], t[0], t[1], t[2])
        	extra_context['semana1']=p

        	q=(p[3], p[0], p[1], p[2])
        	extra_context['semana2']=q

        	w=(q[3], q[0], q[1], q[2])
        	extra_context['semana3']=w
        	
        	r=(w[3], w[0], w[1], w[2])
        	extra_context['semana4']=r


        	s=(r[3], r[0], r[1], r[2])
        	extra_context['semana5']=s

        	tuplaGrupos=[]
        	for g in x.grupos.all():
        		gAux=Grupo.objects.all().filter(nombre=g.nombre)[0]
        		tuplaEmpleados=[g.nombre]
        		for e in gAux.array_empleado.all():
        			tuplaIndividuo=[]
        			repetido = 0
        			for tupGrup in tuplaGrupos:
	        			for tupEmpl in tupGrup:
	        				empleadoAux= e.apellido +", " +e.nombre
	        				if empleadoAux in tupEmpl:
	        					repetido=1
	        					tupEmpl[0]=1		#Si esta repetida, tambien lo cambio en la primer instancia
        			tuplaIndividuo.append(repetido)
        			tuplaIndividuo.append(e.apellido +", " +e.nombre)
        			tuplaEmpleados.append(tuplaIndividuo)
        		#print(tuplaEmpleados)
        		tuplaGrupos.append(tuplaEmpleados)

        	extra_context['grupos']=tuplaGrupos
        	
        return super(GrCalendarioGrupalAdmin, self).changelist_view(request, extra_context=extra_context)

