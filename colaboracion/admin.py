from datetime import *
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import *
admin.site.register(TipoPago)

@admin.register(Pago)
class PagoColaborador(admin.ModelAdmin):
    search_fields = ('colaborador__apellido','id')
    list_display = ('fecha','colaborador','periodo','monto')

@admin.register(PagoProtector)
class PagosProtector(admin.ModelAdmin):
    search_fields = ('protector__nombre','protector__CUIT')
    list_display = ('fecha','protector','periodo','monto')

@admin.register(Colaborador)
class SocioAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'apellido','dni')
    list_display = ('apellido','nombre','dni','direccion','telefono','tipo')
    
@admin.register(Socio_Protector)
class ProtectorAdmin(admin.ModelAdmin):
    search_fields = ('CUIT','razon','nombre')
    list_display = ('nombre','razon','CUIT','direccion','telefono','tipo')

@admin.register(ResumenColaborador)
class PagosAdmin(admin.ModelAdmin):#RESUMEN DE PAGOS DE COLABORADORES
    
    change_list_template = 'admin/resumenPagos.html'
    #date_hierarchy = 'created'
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

            #Para el grafico
        now = datetime.date.today()
        listaGrafico = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for pago in Pago.objects.all():
            if pago.periodo.year == now.year:
                listaGrafico[pago.periodo.month-1] = listaGrafico[pago.periodo.month - 1] + pago.monto
        for pago in PagoProtector.objects.all():
            if pago.periodo.year == now.year:
                listaGrafico[pago.periodo.month-1] = listaGrafico[pago.periodo.month - 1] + pago.monto
        extra_context['sumaPagosMensuales'] = listaGrafico

        lista = [];                #Declaro una lista auxiliar
        for pago in Pago.objects.all().order_by('-periodo'):       #Traigo todos los pagos, con orden de fecha decreciente
            ban = 0
            if len(lista) != 0:                                   #Pongo en lista solo el ultimo pago de cada colaborador
                for l in lista:
                    if l.colaborador == pago.colaborador:
                        ban = 1
            if ban == 0:
                lista.insert(0, pago)
        extra_context['pagos'] = lista                                #En extra_context tendrè solo los ultimos 
            #pagos de cada colaborador
        #Al finalizar la siguiente funcion, obtendremos para cada pago (que representa el ultimo de cada colaborador),
        #un nuevo campo "adeuda" que contendra la diferencia de meses
        for pago in extra_context['pagos']:
            meses = 0
            aux = pago.periodo
            now = datetime.date.today()
            while aux.year < now.year:
                if now.year <= aux.year:
                    break
                meses = meses + 12-aux.month
                aux = aux + datetime.timedelta(years=1)
            while aux.month < now.month:
                meses = meses + 1
                if aux.month == 1:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 2:
                    aux = aux + datetime.timedelta(days=29)
                elif aux.month == 3:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 4:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 5:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 6:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 7:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 8:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 9:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 10:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 11:
                    aux = aux + datetime.timedelta(days=30)
                else:
                    aux = aux + datetime.timedelta(days=31)
                if now.month <= aux.month:
                    break
            pago.adeuda = meses
        # Por ultimo, seria util ver que usuarios registrados no han colaborado nunca.
        laux = []
        for col in Colaborador.objects.all():
            ban = 0
            for pago in extra_context['pagos']:
                if pago.colaborador == col:
                    ban = 1
            if ban == 0:
                laux.append(col)
        extra_context['noColaboraron'] = laux


        #Lo mismo pero para socios protectores

        lista2 = [];                #Declaro una lista auxiliar
        for pago in PagoProtector.objects.all().order_by('-periodo'):       #Traigo todos los pagos, con orden de fecha decreciente
            ban = 0
            if len(lista2) != 0:                                   #Pongo en lista solo el ultimo pago de cada colaborador
                for l in lista2:
                    if l.colaborador == pago.colaborador:
                        ban = 1
            if ban == 0:
                lista2.insert(0, pago)
        extra_context['pagosProtectores'] = lista2                                #En extra_context tendrè solo los ultimos 
            #pagos de cada colaborador
        

        for pago in extra_context['pagosProtectores']:
            meses = 0
            aux = pago.periodo
            now = datetime.date.today()
            while aux.year < now.year:
                if now.year <= aux.year:
                    break
                meses = meses + 12-aux.month
                aux = aux + datetime.timedelta(years=1)
            while aux.month < now.month:
                meses = meses + 1
                if aux.month == 1:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 2:
                    aux = aux + datetime.timedelta(days=29)
                elif aux.month == 3:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 4:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 5:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 6:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 7:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 8:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 9:
                    aux = aux + datetime.timedelta(days=30)
                elif aux.month == 10:
                    aux = aux + datetime.timedelta(days=31)
                elif aux.month == 11:
                    aux = aux + datetime.timedelta(days=30)
                else:
                    aux = aux + datetime.timedelta(days=31)
                if now.month <= aux.month:
                    break
            pago.adeuda = meses



        # Por ultimo, seria util ver que usuarios registrados no han colaborado nunca.
        laux2 = []
        for col in Socio_Protector.objects.all():
            ban = 0
            for pagoProtector in extra_context['pagosProtectores']:
                if pagoProtector.protector == col:
                    ban = 1
            if ban == 0:
                laux2.append(col)
        extra_context['ProtectoresnoColaboraron'] = laux2

        return super(PagosAdmin, self).changelist_view(request, extra_context=extra_context)