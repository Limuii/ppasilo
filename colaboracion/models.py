﻿from django.db import models
from django.core.validators import MinValueValidator
from datetime import date
import datetime
RAZON=(
	('SRL','S.R.L'),
	('SA','S.A'),
	('SAS','S.A.S'),
	('OTRA','OTRA'),
)
# Create your models here.
class TipoPago(models.Model):
    
    descripcion = models.CharField(max_length=25, unique=True, verbose_name="Tipo de pago")

    class Meta:
        verbose_name = "Tipo de pago"
        verbose_name_plural = "Tipo de pagos"

    def __str__(self):
        return self.descripcion


class Colaborador(models.Model):
    
    dni = models.PositiveIntegerField()
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    email = models.EmailField(max_length=255, blank=True, null= True)
    telefono = models.CharField(max_length=20, blank=True, null=True, verbose_name="Teléfono")
    direccion = models.CharField(max_length=50, blank=True, null=True, verbose_name="Dirección")
    tipo = models.ForeignKey(TipoPago, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Socio Colaborador"
        verbose_name_plural = " Socios Colaboradores"

    def __str__(self):
        return self.tipo.__str__() + " - " + self.apellido + " " + self.nombre + " - " + str(self.dni)
    def getnombre():
        return self.apellido + " " + self.nombre

class Socio_Protector(models.Model):
    CUIT = models.PositiveIntegerField()
    razon = models.CharField(max_length=5, choices=RAZON)
    nombre = models.CharField(max_length=50)
    email = models.EmailField(max_length=255, blank=True, null= True)
    telefono = models.CharField(max_length=20, blank=True, null=True, verbose_name="Teléfono")
    direccion = models.CharField(max_length=50, blank=True, null=True, verbose_name="Dirección")
    tipo = models.ForeignKey(TipoPago, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Socio Protector"
        verbose_name_plural = "Socios Protectores"

    def __str__(self):

        return self.tipo.__str__() + " - " + self.nombre + " - " + str(self.CUIT)

class ResumenColaborador(Colaborador):
    class Meta:
        proxy=True
        verbose_name = "Resumen de Colaborador"
        verbose_name_plural = "Resumen de Colaboradores"

class Pago(models.Model):
    periodo = models.DateField()
    fecha = models.DateField(editable=False,default=datetime.datetime.today())
    monto = models.PositiveIntegerField(default=100,validators=[MinValueValidator(100)])
    colaborador = models.ForeignKey(Colaborador, on_delete=models.CASCADE, null=False)

    class Meta:
        verbose_name = "Pago (Socio Colaborador)"
        verbose_name_plural = "Pagos  (Socio Colaborador)"

    def __str__(self):
        time = self.periodo.strftime('%B/%Y')
        return self.colaborador.__str__() + " \t periodo- F: " + time + " - M: " + str(self.monto)

class PagoProtector(models.Model):
    periodo = models.DateField()
    fecha = models.DateField(editable=False,default=datetime.datetime.today())
    monto = models.PositiveIntegerField(default=100,validators=[MinValueValidator(100)])
    protector = models.ForeignKey(Socio_Protector, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Pago (Socio Protector)"
        verbose_name_plural = "Pagos  (Socio Protector)"

    def __str__(self):
        time = self.periodo.strftime('%B/%Y')
        return self.protector.__str__() + " \t periodo- F: " + time + " - M: " + str(self.monto)