from django.db import models

from profesional.models import Profesional
from obrasocial.models import ObraSocial
from hospital.models import Hospital


class Anciano(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.IntegerField(primary_key=True)
    nacimiento = models.DateField(verbose_name="Fecha de nacimiento")
    estadoCivil = models.CharField(max_length=20, verbose_name="Estado civil")

    def __str__(self):
        return self.apellido + " " + self.nombre


class Parentesco(models.Model):
    tipo = models.CharField(max_length=50, verbose_name="Tipo de parentesco", unique=True)

    def __str__(self):
        return self.tipo


class Contacto(models.Model):
    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.IntegerField(primary_key=True)
    domicilio = models.CharField(max_length=255, blank=True, null=True)
    parentesco = models.ForeignKey(Parentesco, on_delete=models.CASCADE)
    telefono1 = models.CharField(verbose_name="Teléfono 1", max_length=20)
    telefono2 = models.CharField(verbose_name="Teléfono 2", max_length=20, blank=True, null=True)
    telefono3 = models.CharField(verbose_name="Teléfono 3", max_length=20, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, null=True)
    lugarTrabajo = models.CharField(verbose_name="Lugar de trabajo", max_length=255, blank=True, null=True)

    def __str__(self):
        return self.apellido + " " + self.nombre


class ActividadFisica(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="Nombre de actividad física")

    class Meta:
        verbose_name = "Actividad física"
        verbose_name_plural = "Actividades físicas"

    def __str__(self):
        return self.nombre


class ActividadDiaria(models.Model):
    ACTIVIDADESVD = (
        ('AutonomiaTotal','Autonomia Total'),
        ('LimitacionesLeves','Limitaciones Leves'),
        ('LimitacionesSeveras','Limitaciones Severas'),
        ('DependenciaTotal','Dependencia Total'),
    )

    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    levantarseAcostarse = models.CharField(max_length=50, verbose_name="Levantarse / Acostarse")
    levantarseAcostarse_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    comidaBebida = models.CharField(max_length=50, verbose_name="Comida / Bebida")
    comidaBebida_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    vestirseDesvestirse = models.CharField(max_length=50, verbose_name="Vestirse / Desvestirse")
    vestirseDesvestirse_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    lavarseArreglarse = models.CharField(max_length=50, verbose_name="Lavarse / Arreglarse")
    lavarseArreglarse_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    bañarseDucharse = models.CharField(max_length=50, verbose_name="Bañarse / Ducharse")
    bañarseDucharse_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    usoSanitarios = models.CharField(max_length=50, verbose_name="Uso sanitarios")
    usoSanitarios_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    vision = models.CharField(max_length=50, verbose_name="Visión")
    vision_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    audicion = models.CharField(max_length=50, verbose_name="Audición")
    audicion_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    orientacionTE = models.CharField(max_length=50, verbose_name="Orientación Temporoesp.")
    orientacionTE_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    desplazamientoVivienda = models.CharField(max_length=50, verbose_name="Desplazamiento en vivienda")
    desplazamientoVivienda_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    relacionEntorno = models.CharField(max_length=50, verbose_name="Relación con el entorno")
    relacionEntorno_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    capacidadAutoproteccion = models.CharField(max_length=50, verbose_name="Capacidad de autoprotección")
    capacidadAutoproteccion_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    capacidadInteraccion = models.CharField(max_length=50, verbose_name="Capacidad de interacción")
    capacidadInteraccion_apoyoFamiliar = models.BooleanField("Apoyo Familiar")
    laboresHogar = models.CharField(max_length=50, verbose_name="Labores del hogar")
    laboresHogar_apoyoFamiliar = models.BooleanField("Apoyo Familiar")

    class Meta:
        verbose_name = "Actividad diaria"
        verbose_name_plural = "Actividades diarias"


class Ingreso(models.Model):
    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    obraSocial = models.ForeignKey(ObraSocial, on_delete=models.CASCADE, verbose_name="Obra social")
    numObraSocial = models.CharField(max_length=20, verbose_name="Número de la obra social") #Char porque son del tipo 31-3434343-4 , despues de ultima definimos unas expresiones regulares
    fechaIngreso = models.DateField(verbose_name="Fecha de ingreso")
    domicilioAlIngreso = models.CharField(max_length=140, verbose_name='Domicilio al momento de ingresar')
    titularDeLaVivienda = models.CharField(max_length=140, verbose_name="Titular de la vivienda")
    viveSolo = models.BooleanField("¿Vive sólo?")
    conQuien = models.CharField(max_length=140, verbose_name="¿Con quién?", null=True, blank=True)
    medicoCabecera = models.ForeignKey(Profesional, on_delete=models.CASCADE, verbose_name="Médico de cabecera")
    lugarInternacion = models.CharField(max_length=140, verbose_name="Lugar de internación", null=True, blank=True)
    pension = models.BooleanField("¿Tiene pensión?")
    numPesion = models.CharField(max_length=20, verbose_name="Número de la pensión", null=True, blank=True) #Char porque son del tipo 31-3434343-4 , despues de ultima definimos unas expresiones regulares
    montoPension = models.IntegerField(verbose_name="Monto de la pensión", null=True, blank=True)
    jubilacion = models.BooleanField(verbose_name="¿Tiene jubilación?")
    numJubilacion = models.CharField(max_length=20, verbose_name="Número de la jubilación", null=True, blank=True) #Char porque son del tipo 31-3434343-4 , despues de ultima definimos unas expresiones regulares
    montoJubilacion = models.IntegerField(verbose_name="Monto de la jubilación", null=True, blank=True)
    # realizaTramite = models.ForeignKey(Contacto, on_delete=models.CASCADE, verbose_name="¿Quién realiza el tramite?")
    motivoInternacionEnfermedadTerminal = models.CharField(max_length=240, verbose_name="Motivo de internación: enfermedad terminal", blank=True, null=True)
    motivoInternacionEnfermedadFisica = models.CharField(max_length=240, verbose_name="Motivo de internación: enfermedad física discapacitante", blank=True, null=True)
    motivoInternacionEnfermedadPsiquica = models.CharField(max_length=240, verbose_name="Motivo de internación: enfermedad psíquica", blank=True, null=True)
    motivoInternacionHabitacional = models.CharField(max_length=240, verbose_name="Motivo de internación: situación habitacional (vivienda)", blank=True, null=True)
    motivoInternacionVulnerabilidadSocial = models.CharField(max_length=240, verbose_name="Motivo de internación: situación de vulnerabilidad social (soledad)", blank=True, null=True)
    situacionSalud = models.TextField(verbose_name="Situación de salud")
    matrimonios = models.IntegerField(null=True, blank=True, default=0)
    cantidadHijos = models.IntegerField(verbose_name="Cantidad de hijos", null=True, blank=True, default=0)
    cantidadHijosVivos = models.IntegerField(verbose_name="Cantidad de hijos vivos", null=True, blank=True, default=0)
    cantidadNietos = models.IntegerField(verbose_name="Cantidad de nietos", null=True, blank=True, default=0)
    profesionOficioAntes = models.CharField(verbose_name="Profesión/Oficio anterior", max_length=50, null=True, blank=True)
    actividadFisica = models.ForeignKey(ActividadFisica, on_delete=models.CASCADE, verbose_name="Actividad física que realiza")
    vecesPorSemana = models.IntegerField(verbose_name="¿Cuántas veces por semana?", null=True, blank=True, default=0)
    acompanado = models.BooleanField(verbose_name="¿Viene acompañado?")
    quienAcompana = models.CharField(verbose_name="¿Quíen lo acompaña?", max_length=50, blank=True, null=True)
    prefiereHoraLevantarse = models.TimeField(verbose_name="¿A qué hora prefiere levantarse?", null=True, blank=True)
    horasDuermeNoche = models.IntegerField(verbose_name="¿Cuántas horas duerme de noche?", null=True, blank=True, default=0)
    horasDuermeDia = models.IntegerField(verbose_name="¿Cuántas de dia?", null=True, blank=True, default=0)
    comidasPreferidas = models.CharField(verbose_name="Comidas preferidas", max_length=200, null=True, blank=True)
    montoPagarMensualmente = models.FloatField(verbose_name="Monto a pagar mensualmente")

    def __str__(self):
        return self.anciano + ' - ' + self.fechaIngreso


class Salida(models.Model):
    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    fechaSalida = models.DateField(verbose_name="Fecha de salida")

    def __str__(self):
        return self.anciano + ' - ' + self.fechaSalida


class Hospitalizacion(models.Model):
    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    fecha = models.DateField(verbose_name="Fecha de hospitalización")
    causa = models.TextField(verbose_name="Causa de hospitalización")
    lugar = models.ForeignKey(Hospital, on_delete=models.CASCADE, verbose_name="Lugar de hospitalización")

    class Meta:
        verbose_name = "Hospitalización"
        verbose_name_plural = "Hospitalizaciones"

    def __str__(self):
        return self.anciano + ' - ' + self.lugar + ' - ' + self.fecha
