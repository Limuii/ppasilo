from django.db import models
from profesional.models import Profesional
from anciano.models import Anciano
from patologia.models import Patologia

# Create your models here.
class Consulta(models.Model):
    anciano = models.ForeignKey(Anciano, on_delete=models.CASCADE)
    profesional = models.ForeignKey(Profesional, on_delete=models.CASCADE)
    fecha = models.DateField()
    patologias = models.ManyToManyField(Patologia, blank=True, verbose_name="Patologías")
    descripcion = models.TextField(max_length=600, blank=True, null=True, verbose_name="Descripción")

    def __str__(self):
        return self.profesional +  ' - ' + self.fecha + ' - ' + self.anciano
