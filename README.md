### Credenciales

user = admin  
pass = asilo1234  

---
### Instalación
    apt-get install python3-venv

### Crear virtual y activarla
    python3 -m venv virtualHogar
    cd virtualHogar
    -linux: source bin/activate
    -windows: venv(C:\Users\name\venv)\Scripts\activate

### Clonar el repositorio
La URL la encuentran en la parte superior derecha, dice Clone.

    git clone https://Limuii@bitbucket.org/Limuii/ppasilo2.git
    python3 -m pip install --upgrade pip


### Instalar requerimientos  
    cd ppasilo
    pip install -r requirements.txt


### Para salir de la virtual
    deactivate
